public class Livre extends Media {
    private String auteur;
    private String ISBN;

    private static String nomMediaheque;

    public Livre(String titre, String cote, int note, String auteur, String ISBN) {
        super(titre, cote, note);
        this.auteur = auteur;
        this.ISBN = ISBN;
    }

    public Livre(Livre m) {
        super(m);
        this.auteur = m.auteur;
        this.ISBN = m.ISBN;
    }

    public Livre() {
        super();
        this.auteur = "";
        this.ISBN = "";
    }

    public String getAuteur()                   { return auteur; }
    public String getISBN()                     { return ISBN; }
    public static String getNomMediaheque()     { return nomMediaheque; }

    void setAuteur(String auteur)           { this.auteur = auteur; }
    void setISBN(String ISBN)               { this.ISBN = ISBN; }
    public static void setNom(String nom)   { nomMediaheque = nom; }

    @Override
    public String toString() {
        return
            "Livre : Titre: " + getTitre() +   // Utilisation des méthodes héritées
            " Auteur: " + auteur +
            " Cote: " + getCote() +            // Utilisation des méthodes héritées
            " Note: " + getNote() +            // Utilisation des méthodes héritées
            " ISBN: " + ISBN +
            " Nom de la médiathèque: " + nomMediaheque;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;

        Livre livre = (Livre) obj;
        return ISBN.equals(livre.ISBN) &&
               auteur.equals(livre.auteur);
    }

    @Override
    public Livre clone() {
        Livre cloned = (Livre) super.clone();
        cloned.auteur = this.auteur;
        cloned.ISBN = this.ISBN;
        return cloned;
    }

    public static void main(String[] args) {
        Livre m1 = new Livre("Le livre de la jungle", "DFTY4HG", 5, "R. Kipling", "2081263246");
        System.out.println(m1);
        m1.setCote("SD77DS");
        System.out.println(m1.getAuteur());
        System.out.println(m1.getAuteur());
    }
}
