public class Film extends Media {
    private String realisateur;
    private int annee;

    public Film() {
        super();
        this.realisateur = "";
        this.annee = 0;
    }

    public Film(String titre, String cote, int note, String realisateur, int annee) {
        super(titre, cote, note);
        this.realisateur = realisateur;
        this.annee = annee;
    }

    public Film(Film f) {
        super(f);
        this.realisateur = f.realisateur;
        this.annee = f.annee;
    }

    public String getRealisateur() { return realisateur; }
    public int getAnnee() { return annee; }

    public void setRealisateur(String realisateur) { this.realisateur = realisateur; }
    public void setAnnee(int annee) { this.annee = annee; }

    @Override
    public String toString() {
        return
            "Film : Titre: " + getTitre() +
            " Cote: " + getCote() +
            " Note: " + getNote() +
            " Realisateur: " + realisateur +
            " Annee: " + annee +
            " Nom de la médiathèque: " + getNomMediatheque();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;

        Film film = (Film) obj;
        return annee == film.annee &&
               realisateur.equals(film.realisateur);
    }

    @Override
    public Film clone() {
        Film cloned = (Film) super.clone();
        cloned.realisateur = this.realisateur;
        cloned.annee = this.annee;
        return cloned;
    }

    public static void main(String[] args) {
        Film f1 = new Film("Inception", "3DXX7", 5, "Christopher Nolan", 2010);
        System.out.println(f1);

        Film f2 = f1.clone();
        System.out.println("Film cloné : " + f2);

        f2.setRealisateur("Quentin Tarantino");
        f2.setAnnee(2003);

        System.out.println("Film original : " + f1);
        System.out.println("Film modifié : " + f2);

        System.out.println("Films égaux ? " + f1.equals(f2));
    }
}
