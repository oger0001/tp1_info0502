import java.util.Vector;

public class Mediatheque {
    private String nomProprietaire;
    private Vector<Media> archive;

    public Mediatheque() {
        nomProprietaire = "";
        archive = new Vector<Media>();
    }

    public Mediatheque(Mediatheque m) {
        this.nomProprietaire = m.nomProprietaire;
        this.archive = new Vector<>();

        for (Media media : m.archive) {
            this.archive.add(media.clone());
        }
    }

    public void add(Media m) { archive.add(m); }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Mediatheque : ");
        sb.append("Propriétaire: ").append(nomProprietaire).append("\n");

        sb.append("Archive :\n");

        for (Media media : archive) { sb.append(media.toString()).append("\n"); }

        return sb.toString();
    }

    public static void main(String[] args) {
        Livre livre1 = new Livre("1984", "B1F3T", 5, "George Orwell", "9780451524935");
        Film film1 = new Film("Inception", "A1B2C", 4, "Christopher Nolan", 2010);
        Media media1 = new Media("The Dark Side of the Moon", "X5Y7Z", 5);

        Mediatheque mediatheque = new Mediatheque();

        mediatheque.add(livre1);
        mediatheque.add(film1);
        mediatheque.add(media1);

        Media.setNom("Les Livre du Zodiac");

        System.out.println(mediatheque);
    }
}
