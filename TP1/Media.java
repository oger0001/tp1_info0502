public class Media implements Cloneable {
    private String titre;
    private StringBuffer cote;
    private int note;

    private static String nomMediatheque;

    public Media(String titre, String cote, int note) {
        this.titre = titre;
        this.cote = new StringBuffer(cote);
        this.note = note;
    }

    public Media(Media m) {
        this.titre = m.titre;
        this.cote = m.cote;
        this.note = m.note;
    }

    public Media() {
        this.titre = "";
        this.cote = new StringBuffer("");
        this.note = 0;
    }

    public StringBuffer getCote()               { return cote; }
    public String getTitre()                    { return titre; }
    public int getNote()                        { return note; }
    public static String getNomMediatheque()    { return nomMediatheque; }

    void setCote(String cote)               { this.cote = new StringBuffer(cote); }
    void setTitre(String titre)             { this.titre = titre; }
    void setNote(int note)                  { this.note = note; }
    public static void setNom(String nom)   { nomMediatheque = nom; }

    public String toString() {
        return
            "Media : Titre: " + titre +
            " Cote: " + cote +
            " Note: " + note +
            " Nom de la médiathèque: " + nomMediatheque;
    }

    public boolean equals(Media m) {
        return
            this.cote.toString().equals(m.cote.toString()) &&
            this.titre.equals(m.titre) &&
            this.note == m.note;
    }

    @Override
    public Media clone() {
        try {
            Media cloned = (Media) super.clone();
            cloned.cote = new StringBuffer(this.cote);
            return cloned;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

    public static void main(String[] args) {
        Media m1 = new Media("Kill Bill vol 1","2BXX5",5);
        Media.setNom("Les Livres du Zodiac");
        System.out.println(m1);
        m1.getCote().reverse();
        System.out.println(m1);
    }
}